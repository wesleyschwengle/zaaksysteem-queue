#!/usr/bin/env perl

use warnings;
use strict;

use Log::Log4perl ':easy';
use Zaaksysteem::Queue::Watcher;

Log::Log4perl->easy_init($DEBUG);

Zaaksysteem::Queue::Watcher->new_with_options->run;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
