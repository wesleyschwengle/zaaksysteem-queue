# Build this using: docker build --label=zaaksysteem/zs-queue:latest .
FROM perl:5.22

COPY . /usr/local/src/zsqueue
WORKDIR /usr/local/src/zsqueue

RUN    apt-get update \
    && apt-get -y install libpq-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && perl Makefile.PL \
    && cpanm --installdeps . \
    && rm -rf ~/.cpanm \
    && make install \
    && useradd -ms /bin/bash zaaksysteem -d /opt/zaaksysteem \
    && chown -R zaaksysteem /opt/zaaksysteem

USER zaaksysteem
CMD [ "/usr/local/bin/zs-queue.pl", "--customer_d", "/etc/zaaksysteem/customer.d" ]
